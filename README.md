# README #

### Prerequistes ###

You will need the following things properly installed on your own machine:

* Java 8

### Set-up ###

If you do not have a Gradle installed you might use this commands

```
#!

chmod +x gradlew
./gradlew clean build
```

But if you already do, you can run the follow one

```
#!

gradle build
```

### Running ###

You can build and run this application as a standalone JAR (through gradle or using the JAR) or as a WAR that is deployable to WildFly.

#### Run using gradle ####

Execute

```
#!

gradle bootRun
```

#### Run as a standalone JAR ####

Execute

```
#!

gradle clean build
java -jar build/libs/mars10.jar
```

#### Build WAR and deploy to WildFly ####

Uncomment some lines related to WAR configurations in build.gradle:

```
#!groovy

apply plugin: 'war'
```

```
#!groovy

war {
    baseName = baseName
    version =  version
}
```

```
#!groovy

providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
```

Start WildFly


```
#!

sh <wildfly directory>/bin/standalone.sh
```


And then move the WAR package to the deployments folder of WildFly


```
#!

mv build/libs/mars10.war <wildfly directory>/standalone/deployments/
```

**Attention:** When deployed in the WildFly, the endpoint will change to /mars10/rest/mars/ adding the prefix /mars10/ that is the application base name.

### Usage ###

#### Movimento com rotações para direita: #### 

```
#!

curl -s --request POST http://localhost:8080/rest/mars/MMRMMRMM
```

Saída esperada: (2, 0, S)

#### Movimento para esquerda: ####

```
#!

curl -s --request POST http://localhost:8080/rest/mars/MML
```

Saída esperada: (0, 2, W)

#### Posição inválida: ####

```
#!

curl -s --request POST http://localhost:8080/rest/mars/MMMMMMMMMMMMMMMMMMMMMMMM
```

Saída esperada: 400 Bad Request