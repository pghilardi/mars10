package app.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestRobotController {

    @Autowired
    private MockMvc mvc;

    @Test
    public void movementsToRight() throws Exception {
        String endpoint = "/rest/mars/MMRMMRMM";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();

        Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        String content = result.getResponse().getContentAsString();
        Assert.assertEquals("(2, 0, S)", content);
    }

    @Test
    public void movementsDoNotKeepState() throws Exception {
        String endpoint = "/rest/mars/MML";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();

        Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        String content = result.getResponse().getContentAsString();
        Assert.assertEquals("(0, 2, W)", content);

        result = mvc.perform(MockMvcRequestBuilders.post(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();

        Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        content = result.getResponse().getContentAsString();
        Assert.assertEquals("(0, 2, W)", content);
    }

    @Test
    public void outOfBoundsMovementReturnsBadRequest() throws Exception {
        String endpoint = "/rest/mars/MMMMMMMMMMMMMMMMMMMMMMMM";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());

        String errorMessage = result.getResponse().getErrorMessage();
        Assert.assertEquals("The position is out of bounds", errorMessage);
    }

    @Test
    public void emptyActionsReturnsNotFound() throws Exception {
        String endpoint = "/rest/mars/";
        mvc.perform(MockMvcRequestBuilders.post(endpoint).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void invalidActionsReturnBadRequest() throws Exception {
        String endpoint = "/rest/mars/AAA";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());

        String errorMessage = result.getResponse().getErrorMessage();
        Assert.assertEquals("Invalid actions. The valid actions are: R, L or M", errorMessage);
    }

    @Test
    public void lowerCaseIsAnInvalidAction() throws Exception {
        String endpoint = "/rest/mars/llm";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();
        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());

        String errorMessage = result.getResponse().getErrorMessage();
        Assert.assertEquals("Invalid actions. The valid actions are: R, L or M", errorMessage);
    }

    @Test
    public void onlyRotationMovementsDoesNotChangePositionOnlyDirection() throws Exception {
        String endpoint = "/rest/mars/LLLLRRRRRRRRRLLRRRRLLLLRRRL";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();

        Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

        String content = result.getResponse().getContentAsString();
        Assert.assertEquals("(0, 0, E)", content);
    }

    @Test
    public void complexMovements() throws Exception {
        String endpoint = "/rest/mars/MMMRMMRMMLMMLMM";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();

        Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

        String content = result.getResponse().getContentAsString();
        Assert.assertEquals("(4, 3, N)", content);
    }

}
