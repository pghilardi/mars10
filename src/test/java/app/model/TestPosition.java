package app.model;

import app.model.exception.InvalidDirectionException;
import app.model.exception.InvalidPositionException;
import app.model.exception.PositionAlreadyFilledException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

public class TestPosition {

    @Test
    public void testAPositionWithRobotIsNotEmpty() throws InvalidPositionException, PositionAlreadyFilledException, InvalidDirectionException {
        Position position = new Position(1, 1);
        Robot robot = new Robot();
        Assert.assertTrue(position.isEmpty());

        position.put(robot);
        Assert.assertFalse(position.isEmpty());

        position.release();
        Assert.assertTrue(position.isEmpty());
    }
}
