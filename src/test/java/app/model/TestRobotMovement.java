package app.model;

import app.model.exception.InvalidDirectionException;
import app.model.exception.InvalidPositionException;
import app.model.exception.PositionAlreadyFilledException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

public class TestRobotMovement {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testOnlyRotateRobotStayInTheSamePosition() throws InvalidPositionException, PositionAlreadyFilledException, InvalidDirectionException {
        RobotMovement movement = RobotMovementFactory.create(5, 5);
        ArrayList<Action> actions = new ArrayList<>();
        actions.add(Action.LEFT);
        actions.add(Action.LEFT);
        actions.add(Action.LEFT);
        actions.add(Action.RIGHT);
        actions.add(Action.RIGHT);
        actions.add(Action.RIGHT);
        actions.add(Action.LEFT);
        actions.add(Action.RIGHT);
        actions.add(Action.LEFT);
        actions.add(Action.RIGHT);
        actions.add(Action.RIGHT);
        actions.add(Action.RIGHT);

        RobotMovementOutput output = movement.execute(actions);

        Position expectedPosition = new Position(0, 0);
        Assert.assertEquals(Direction.SOUTH, output.direction);
        Assert.assertEquals(expectedPosition.getX(), output.position.getX());
        Assert.assertEquals(expectedPosition.getY(), output.position.getY());
    }

    @Test
    public void testValidMovementWithRotations() throws InvalidPositionException, PositionAlreadyFilledException, InvalidDirectionException {
        RobotMovement movement = RobotMovementFactory.create(5, 5);
        ArrayList<Action> actions = new ArrayList<>();
        actions.add(Action.MOVE);
        actions.add(Action.MOVE);
        actions.add(Action.RIGHT);
        actions.add(Action.RIGHT);
        actions.add(Action.LEFT);
        actions.add(Action.LEFT);
        actions.add(Action.MOVE);
        actions.add(Action.LEFT);
        RobotMovementOutput output = movement.execute(actions);

        Position expectedPosition = new Position(0, 3);
        Assert.assertEquals(Direction.WEST, output.direction);
        Assert.assertEquals(expectedPosition.getX(), output.position.getX());
        Assert.assertEquals(expectedPosition.getY(), output.position.getY());
    }

    @Test
    public void testMoveOutsideOfBoundsThrowsException() throws InvalidPositionException, PositionAlreadyFilledException, InvalidDirectionException {
        RobotMovement movement = RobotMovementFactory.create(5, 5);
        ArrayList<Action> actions = new ArrayList<>();
        actions.add(Action.MOVE);
        actions.add(Action.MOVE);
        actions.add(Action.MOVE);
        actions.add(Action.MOVE);
        actions.add(Action.MOVE);

        thrown.expect(InvalidPositionException.class);
        thrown.expectMessage("The position (0, 5) is out of the valid bounds (4, 4)");
        movement.execute(actions);
    }

}
