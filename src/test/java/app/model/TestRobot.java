package app.model;

import app.model.exception.InvalidDirectionException;
import app.model.exception.InvalidPositionException;
import app.model.exception.PositionAlreadyFilledException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

public class TestRobot {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testRobotMovementsFromOnePositionToAnother() throws InvalidPositionException, PositionAlreadyFilledException, InvalidDirectionException {
        Map map = new Map(5, 5);
        Robot robot = new Robot();

        Position startPosition = map.getPosition(0, 0);
        robot.setCurrentPosition(startPosition);

        robot.move(map, Action.MOVE);
        robot.move(map, Action.MOVE);
        robot.move(map, Action.MOVE);
        robot.move(map, Action.RIGHT);
        robot.move(map, Action.RIGHT);

        Position expectedPosition = new Position(0, 3);
        Position robotPosition = robot.getPosition();
        Direction robotDirection = robot.getDirection();
        Assert.assertEquals(expectedPosition.getX(), robotPosition.getX());
        Assert.assertEquals(expectedPosition.getY(), robotPosition.getY());
        Assert.assertEquals(Direction.SOUTH, robotDirection);

        robot.move(map, Action.MOVE);
        robot.move(map, Action.LEFT);

        expectedPosition = new Position(0, 2);
        robotPosition = robot.getPosition();
        robotDirection = robot.getDirection();
        Assert.assertEquals(expectedPosition.getX(), robotPosition.getX());
        Assert.assertEquals(expectedPosition.getY(), robotPosition.getY());
        Assert.assertEquals(Direction.EAST, robotDirection);
    }

    @Test
    public void testMoveToNotEmptyPositionThrowsException() throws InvalidPositionException, PositionAlreadyFilledException, InvalidDirectionException {
        Map map = new Map(5, 5);
        Robot robot = new Robot();

        Position startPosition = map.getPosition(0, 0);
        robot.setCurrentPosition(startPosition);

        robot.move(map, Action.MOVE);
        robot.move(map, Action.MOVE);

        Robot otherRobot = new Robot();
        otherRobot.setCurrentPosition(startPosition);

        otherRobot.move(map, Action.MOVE);

        thrown.expect(PositionAlreadyFilledException.class);
        thrown.expectMessage("The position (0, 2) is not empty");
        otherRobot.move(map, Action.MOVE);

    }

    @Test
    public void testMoveRobotToOutOfBoundsThrowsException() throws InvalidPositionException, PositionAlreadyFilledException, InvalidDirectionException {
        Map map = new Map(2, 2);
        Robot robot = new Robot();

        Position startPosition = map.getPosition(0, 0);
        robot.setCurrentPosition(startPosition);

        robot.move(map, Action.MOVE);

        thrown.expect(InvalidPositionException.class);
        thrown.expectMessage("The position (0, 2) is out of the valid bounds (1, 1)");
        robot.move(map, Action.MOVE);

    }
}
