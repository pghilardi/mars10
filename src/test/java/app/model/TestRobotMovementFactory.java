package app.model;

import app.model.exception.InvalidPositionException;
import app.model.exception.PositionAlreadyFilledException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestRobotMovementFactory {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testInvalidPositionOnCreation() throws InvalidPositionException, PositionAlreadyFilledException {
        thrown.expect(InvalidPositionException.class);
        thrown.expectMessage("Cannot create robot with negative bounds");
        RobotMovementFactory.create(-1, -1);
    }

}
