package app.model;

import app.model.exception.InvalidPositionException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestMap {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGetPositionReturnsAValidPosition() throws InvalidPositionException {
        Map map = new Map(5, 5);
        Position position = map.getPosition(3, 2);
        Assert.assertEquals(3, position.getX());
        Assert.assertEquals(2, position.getY());
    }

    @Test
    public void testGetPositionOutOfBoundsThrowsException() throws InvalidPositionException {
        Map map = new Map(5, 5);
        thrown.expect(InvalidPositionException.class);
        thrown.expectMessage("The position (3, 20) is out of the valid bounds (4, 4)");

        map.getPosition(3, 20);
    }
}
