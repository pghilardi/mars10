package app.controller;

import app.configuration.EndPointConfiguration;
import app.model.Action;
import app.model.RobotMovement;
import app.model.RobotMovementFactory;
import app.model.RobotMovementOutput;
import app.model.exception.InvalidDirectionException;
import app.model.exception.InvalidPositionException;
import app.model.exception.PositionAlreadyFilledException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.IOException;
import java.util.ArrayList;

@RestController
@Validated
public class RobotController {

    public static final int DEFAULT_M = 5;
    public static final int DEFAULT_N = 5;

    @RequestMapping(value = EndPointConfiguration.MARS, method = RequestMethod.POST)
    public String action(@Valid @Pattern(regexp = "(R|L|M)*") @PathVariable String actions) throws PositionAlreadyFilledException, InvalidPositionException, InvalidDirectionException {

        RobotMovement movement = RobotMovementFactory.create(DEFAULT_M, DEFAULT_N);
        ArrayList<Action> robotActions = new ArrayList<>();
        for(char action : actions.toCharArray()) {
            robotActions.add(Action.get(String.valueOf(action)));
        }
        RobotMovementOutput output = movement.execute(robotActions);
        return output.toString();
    }

    @ExceptionHandler(ConstraintViolationException.class)
    void handleInvalidActions(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Invalid actions. The valid actions are: R, L or M");
    }

}
