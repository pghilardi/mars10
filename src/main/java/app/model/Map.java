package app.model;

import app.model.exception.InvalidPositionException;
import app.model.exception.PositionAlreadyFilledException;

/**
 * A 2D map in mars with a set of positions.
 */
public class Map {

    private Position[][] positions;

    private int m;

    private int n;

    /**
     * Constructs a 2D map (m x n)
     * @param m
     * @param n
     */
    public Map(int m, int n){
        this.m = m;
        this.n = n;
        this.positions =  new Position[m][n];
        for (int i=0; i<m; i++){
            for (int j=0; j<n; j++){
                this.positions[i][j] = new Position(i, j);
            }
        }
    }

    /**
     * Check if the position is a valid one (inside the bounds)
     * @param x
     * @param y
     * @throws InvalidPositionException when the position is out of the bounds
     */
    private void checkIsValidPosition(int x, int y) throws InvalidPositionException {
        if ((x < 0 || x >= m) || (y < 0 || y >= n)){
            String message = String.format("The position (%s, %s) is out of the valid bounds (%s, %s)", x, y, m - 1, n - 1);
            throw new InvalidPositionException(message);
        }
    }

    /**
     * Check if the position is already filled by other robot
     * @param x
     * @param y
     * @throws PositionAlreadyFilledException when the position is already filled
     */
    private void checkIsEmptyPosition(int x, int y) throws PositionAlreadyFilledException {
        if (!this.positions[x][y].isEmpty()) {
            String message = String.format("The position (%s, %s) is not empty", x, y);
            throw new PositionAlreadyFilledException(message);
        }
    }

    /**
     *
     * @param x
     * @param y
     * @return Position the position in these coordinates
     * @throws InvalidPositionException when the position requested is invalid (out of bounds)
     */
    public Position getPosition(int x, int y) throws InvalidPositionException {
        checkIsValidPosition(x, y);
        return this.positions[x][y];
    }

    /**
     *
     * @param x
     * @param y
     * @throws InvalidPositionException when the position requested is invalid (out of bounds)
     * @throws PositionAlreadyFilledException when the position is already filled by other robot
     */
    public void checkPosition(int x, int y) throws InvalidPositionException, PositionAlreadyFilledException {
        checkIsValidPosition(x, y);
        checkIsEmptyPosition(x, y);
    }
}
