package app.model;

/**
 * A position inside a map. A position may be empty or filled by a robot.
 */
public class Position {

    private int x;
    private int y;
    private Robot robot;

    /**
     * A position on coordinates (x, y)
     * @param x
     * @param y
     */
    public Position(int x, int y){
        this.x = x;
        this.y = y;
    }

    /**
     * Put a robot in this position
     * @param robot The robot to fill this position
     */
    public void put(Robot robot){
        this.robot = robot;
        this.robot.setCurrentPosition(this);
    }

    /**
     * Release this position to empty
     */
    public void release(){
        this.robot = null;
    }

    /**
     *
     * @return true if the position is empty, false otherwise
     */
    public boolean isEmpty() {
        return this.robot == null;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
