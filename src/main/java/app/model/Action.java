package app.model;

import java.util.EnumSet;
import java.util.HashMap;

/**
 * Acions that can be executed by a robot: LEFT, RIGHT, MOVE
 */
public enum Action {

    LEFT("L"), RIGHT("R"), MOVE("M");

    private static final HashMap<String, Action> lookup = new HashMap<>();

    static {
        for(Action s : EnumSet.allOf(Action.class))
            lookup.put(s.getShortName(), s);
    }

    private String shortName;

    Action(String actionShortName) {
        this.shortName = actionShortName;
    }

    public String getShortName() { return shortName; }

    public static Action get(String actionShortName) {
        return lookup.get(actionShortName);
    }
}
