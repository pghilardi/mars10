package app.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="The position is out of bounds")
public class InvalidPositionException extends Exception {

    public InvalidPositionException(String message) {
        super(message);
    }

}
