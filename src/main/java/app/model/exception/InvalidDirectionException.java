package app.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Invalid direction")
public class InvalidDirectionException extends Throwable {

    public InvalidDirectionException(String message) {
        super(message);
    }

}
