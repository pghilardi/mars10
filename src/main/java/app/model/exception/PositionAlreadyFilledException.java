package app.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="The position is not empty")
public class PositionAlreadyFilledException extends Exception {

    public PositionAlreadyFilledException(String message) {
        super(message);
    }

}
