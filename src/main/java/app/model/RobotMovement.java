package app.model;

import app.model.exception.InvalidDirectionException;
import app.model.exception.InvalidPositionException;
import app.model.exception.PositionAlreadyFilledException;

import java.util.ArrayList;

/**
 * Executes a set of actions to move/rotate a robot on a map in mars.
 */
public class RobotMovement {

    private Map map;
    private Robot robot;

    /**
     * Creates a robot movement controller that set-ups a map and a robot and allows
     * to execute some actions with the robot
     *
     * @param m The map m boundary
     * @param n The map n boundary
     * @throws InvalidPositionException
     */
    public RobotMovement(int m, int n) throws InvalidPositionException {
        this.map = new Map(m, n);
        this.robot = new Robot();
        Position position = this.map.getPosition(0, 0);
        position.put(this.robot);
    }

    /**
     *
     * @param actions The actions to be executed by the robot in the map
     * @return The movement output: (x, y, Direction)
     *
     * @throws PositionAlreadyFilledException when the position is already filled by other robot
     * @throws InvalidPositionException when is an invalid position (out of bounds)
     * @throws InvalidDirectionException when the direction sent is an invalid direction
     */
    public RobotMovementOutput execute(ArrayList<Action> actions) throws InvalidPositionException, PositionAlreadyFilledException, InvalidDirectionException {
        for (Action action : actions) {
            robot.move(map, action);
        }
        RobotMovementOutput output = new RobotMovementOutput();
        output.direction = robot.getDirection();
        output.position = robot.getPosition();
        return output;
    }

}
