package app.model;

public class RobotMovementOutput {

    private static final String ROBOT_MOVEMENT_OUTPUT_FORMAT = "(%s, %s, %s)";

    public Position position;

    public Direction direction;

    public String toString(){
        return String.format(
            ROBOT_MOVEMENT_OUTPUT_FORMAT,
            this.position.getX(),
            this.position.getY(),
            this.direction.getShortName()
        );
    }

}
