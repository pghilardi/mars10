package app.model;

import java.util.EnumSet;
import java.util.HashMap;

/**
 * Directions of a robot: SOUTH, NORTH, EAST, WEST
 */
public enum Direction {

    SOUTH("S"), NORTH("N"), EAST("E"), WEST("W");

    private static final HashMap<String, Direction> lookup = new HashMap<>();

    static {
        for(Direction s : EnumSet.allOf(Direction.class))
            lookup.put(s.getShortName(), s);
    }

    private String shortName;

    Direction(String directionShortName) {
        this.shortName = directionShortName;
    }

    public String getShortName() { return shortName; }

    public static Direction get(String directionShortName) {
        return lookup.get(directionShortName);
    }

}
