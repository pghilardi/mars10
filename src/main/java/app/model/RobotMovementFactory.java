package app.model;

import app.model.exception.InvalidPositionException;

/**
 * Factory to create robot movements objects
 */
public class RobotMovementFactory {

    /**
     *
     * @param m The m coordinate
     * @param n The n coordinate
     * @return The robot movement object
     * @throws InvalidPositionException when is an invalid position (out of bounds)
     */
    public static RobotMovement create(int m, int n) throws InvalidPositionException {

        if (m < 0 || n < 0){
            throw new InvalidPositionException("Cannot create robot with negative bounds");
        }

        return new RobotMovement(m, n);
    }

}
