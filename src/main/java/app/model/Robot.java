package app.model;

import app.model.exception.InvalidDirectionException;
import app.model.exception.InvalidPositionException;
import app.model.exception.PositionAlreadyFilledException;

import java.util.ArrayList;

/**
 * A robot has two main actions: rotate (LEFT or RIGHT) and move one step (MOVE) in
 * the current robot direction.
 */
public class Robot {

    private Position currentPosition;

    private ArrayList<Direction> directions = new ArrayList<>(4);

    private Direction currentDirection;

    public Robot(){
        directions.add(Direction.NORTH);
        directions.add(Direction.EAST);
        directions.add(Direction.SOUTH);
        directions.add(Direction.WEST);
        currentDirection = Direction.NORTH;
    }

    /**
     *
     * @param action The action to be executed by the robot
     * @return Direction
     *  The new direction of the robot after the action is executed
     */
    private Direction rotate(Action action){
        int offset = action.equals(Action.LEFT) ? -1 : 1;
        int index = directions.indexOf(currentDirection) + offset;
        if (index < 0){
            index = directions.size() - 1;
        } else if (index >= directions.size()){
            index = 0;
        }
        return directions.get(index);
    }

    /**
     * Move the robot in a map
     *
     * @param map The map
     * @param action The action to be executed
     *
     * @throws PositionAlreadyFilledException when the position is already filled by other robot
     * @throws InvalidPositionException when is an invalid position (out of bounds)
     * @throws InvalidDirectionException when the direction sent is an invalid direction
     */
    public void move(Map map, Action action) throws PositionAlreadyFilledException, InvalidPositionException, InvalidDirectionException {
        if (action.equals(Action.LEFT) || action.equals(Action.RIGHT)){
            currentDirection = this.rotate(action);
        } else if (action.equals(Action.MOVE)){
            currentPosition = this.moveOneStep(map);
        }
    }

    /**
     *
     * @param map The map
     * @return Position The new robot position after the move
     * @throws PositionAlreadyFilledException when the position is already filled by other robot
     * @throws InvalidPositionException when is an invalid position (out of bounds)
     * @throws InvalidDirectionException when the direction sent is an invalid direction
     */
    private Position moveOneStep(Map map) throws InvalidDirectionException, InvalidPositionException, PositionAlreadyFilledException {
        int currentX = currentPosition.getX();
        int currentY = currentPosition.getY();

        int nextX = currentX;
        int nextY = currentY;
        if (currentDirection.equals(Direction.NORTH)){
            nextY = currentY + 1;
        } else if (currentDirection.equals(Direction.SOUTH)){
            nextY = currentY - 1;
        } else if (currentDirection.equals(Direction.EAST)){
            nextX = currentX + 1;
        } else if (currentDirection.equals(Direction.WEST)){
            nextX = currentX - 1;
        } else {
            throw new InvalidDirectionException("Invalid direction on map");
        }

        map.checkPosition(nextX, nextY);
        Position nextPosition = map.getPosition(nextX, nextY);

        currentPosition.release();
        nextPosition.put(this);

        return nextPosition;
    }

    /**
     * @return Direction The current robot direction
     */
    public Direction getDirection(){
        return currentDirection;
    }

    /**
     * @return Position The current robot position
     */
    public Position getPosition(){
        return currentPosition;
    }

    /**
     * @param currentPosition The position to be set
     */
    public void setCurrentPosition(Position currentPosition) {
        this.currentPosition = currentPosition;
    }
}
